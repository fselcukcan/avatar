/*
-App
  -Avatar
  -Avatar
*/
var React = require('react');
var ReactDOM = require('react-dom');

var Avatar = React.createClass({
  render: function() {
    return (
      <div style={this.styles}>
        <img src={this.props.image} style={this.styles} />
      </div>
    );
  },
  styles: {
      width: 100,
      height: 100,
      borderRadius: 50,
      background: 'teal',
  },
});

var App = React.createClass({
  render: function() {
    var imageNames = [1,2,3,4,5,6];
    var nodes = [];
    imageNames.forEach(function(imageName) {
      var imageUrl = 'img/'+imageName+'.jpg';
      nodes.push(<Avatar image={imageUrl} />);
    });
    return (
      <div>
        {nodes}
      </div>
    );
  },
});

ReactDOM.render(<App />, document.getElementById('avatar-app'));
